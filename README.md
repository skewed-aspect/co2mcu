# CO2MCU

This are all the the project files required to build a basic smart CO2 sensor. The goal is to build one which work with [Home Assistant][ha].

## Development

Development is done using [ESPHome][]. To flash the device, simply run:

```bash
$ esphome run co2mcu.yml
```

## Parts

* NodeMCU: https://www.amazon.com/HiLetgo-Internet-Development-Wireless-Micropython/dp/B081CSJV2V/ref=sr_1_3?dchild=1&keywords=nodemcu&qid=1631412517&sr=8-3
* CO2 Sensor: https://www.digikey.com/en/products/detail/sensirion-ag/SCD30/8445334
* ProtoBoard : https://www.adafruit.com/product/4784

[ha]: https://www.home-assistant.io/
[esphome]: https://esphome.io/

## Schematic

![Handdrawn Schematic](images/nodemcu_schem_handdrawn.jpg)

The sensor takes `3.3v`, which the NodeMCU supplies. Then you simply need to connect the `TX` and `RX` lines, and tie `SEL` to ground.

| NodeMCU Pin | SDC30 Pin  | Description   |
|:-----------:|:----------:|:-------------:|
| 10 (GND)    | 2 (GND)    | Ground        |
| 14 (GND)    | 7 (SEL)    | Select        |
| 16 (3V3)    | 1 (VIN)    | 3.3v Input    |
| 28 (D2)     | 4 (RX/SDA) | Receive Line  |
| 29 (D1)     | 3 (TX/SCL) | Transmit Line |

Pins `5` and `6` on the `SDC30` sensor are left unconnected.

## Build

For the actual build, we used a prototype board [from adafruit](https://www.adafruit.com/product/4784), and simply soldered wires to make all the connections.

![Adafruit Protoboard](images/co2mcu_real_diag.png)

Using this layout, the connections that need to be made are:

| NodeMCU Pin | SDC30 Pin  | Colors.      | Notes              |
|:-----------:|:----------:|:------------:|:-------------------|
| A02         | X02        | Red/White    | VIN (Underside)    |
| B09         | R02        | Green        | SEL                |
| F09         | W02        | Green        | GND                |
| N02         | V02        | Yellow       | TX/SCL             |
| M02         | U02        | Purple/White | RX/SDA (Underside) |

The two wires with white dashed lines are wired on the underside of the board.